import os
import uproot
import awkward as ak
import pickle
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import vector
vector.register_awkward()

# Uproot ntuple produced from HGCAL L1T TPG software
files = {}

# 200 PU
#files['simulation'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_simulation_pass2/ntuple_simulation.root"
#files['emulator'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_emulator_pass2/ntuple_emulator.root"
#files['emulator_LT'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_emulator_pass2_lowerThreshold/ntuple_emulator.root"
#files['emulator_flat'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_emulator_pass3_flatThreshold/ntuple_emulator.root"

# 0 PU
files['simulation'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_NoPU_simulation_pass2/ntuple_simulation.root"
files['emulator'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_NoPU_emulator_pass2/ntuple_emulator.root"
#files['emulator_flat'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_NoPU_emulator_pass3_flatThreshold/ntuple_emulator.root"


save_collections = False
load_collections = False

do_cl3d_plots = False
# Options for do_cl3d_plots
norm_by_events = False
norm_by_area = False

do_sb_cl3d_plots = False

do_efficiency_plot = False

do_resolution_plot = False

do_ptresponse_plot = False

do_sandbox_plot = False

# Plotting details
#output_dir = "./plots_ptcut2_NoPU_flatThreshold"
output_dir = "./plots_ptcut2_PU200_flatThreshold"
if do_cl3d_plots | do_sb_cl3d_plots | do_efficiency_plot | do_resolution_plot | do_ptresponse_plot | do_sandbox_plot:
    if not os.path.isdir(output_dir):
        os.system("mkdir -p %s"%output_dir)

plotting_details = {
    "resolution":{
        "pt_bins" : np.linspace(2,100,15,dtype='int'),
        "nbins" : 20,
        "mean_yrange" : (-0.25,0.75),
        "std_yrange" : (0,0.75)
    },
    "efficiency_pt":{
        #"bins" : np.linspace(0,25,26)
        "bins" : np.linspace(2,100,15)
    },
    "efficiency_eta":{
        "bins" : np.linspace(1.5,3.0,16)
    },
    "efficiency_phi":{
        "bins" : np.linspace(-3.15,3.15,16)
    },
    "pt":{
        "nbins" : 40,
        "xrange" : [0,100],
        "logy" : True
    },
    "energy":{
        "nbins" : 40,
        "xrange" : [0,200],
        "logy" : True
    },
    "firstlayer":{
        "nbins" : 10,
        "xrange" : [0,10],
        "logy" : False
    },
    "showerlength":{
        "nbins" : 52,
        "xrange" : [0,52],
        "logy" : False
    },
    "coreshowerlength":{
        "nbins" : 30,
        "xrange" : [0,30],
        "logy" : False
    },
    "hoe":{
        "nbins" : 40,
        "xrange" : [0,1],
        "logy" : False
    },
    "layer10":{
        "nbins" : 50,
        "xrange" : [0,50],
        "logy" : False
    },
    "layer50":{
        "nbins" : 50,
        "xrange" : [0,50],
        "logy" : False
    },
    "layer90":{
        "nbins" : 50,
        "xrange" : [0,50],
        "logy" : False
    },
    "bdteg":{
        "nbins" : 40,
        "xrange" : [-1,1],
        "logy" : True
    },
    "sigmaE":{
        "nbins" : 50,
        "xrange" : [0,5000],
        "logy" : False
    },
    "sigmaPhi":{
        "nbins" : 50,
        "xrange" : [0,100],
        "logy" : False
    },
    "sigmaRoZ":{
        "nbins" : 50,
        "xrange" : [0,200],
        "logy" : False
    }

}

color_map = {
    "simulation":"deepskyblue",
    "simulation_ngm":"midnightblue",
    "emulator":"lightcoral",
    "emulator_ngm":"darkred",
    "emulator_LT":"forestgreen",
    "emulator_flat":"forestgreen"
}

variables = {
    "simulation":{
        "gen":["pt","eta","phi","energy"],
        "cl3d":['pt', 'energy', 'eta', 'phi', 'quality', 'showerlength', 'coreshowerlength', 'firstlayer', 'maxlayer', 'seetot', 'seemax', 'spptot', 'sppmax', 'szz', 'srrtot', 'srrmax', 'srrmean', 'emaxe', 'hoe', 'meanz', 'layer10', 'layer50', 'layer90', 'ntc67', 'ntc90', 'bdteg', 'quality']
    },
    "emulator":{
        "gen":["pt","eta","phi","energy"],
        "cl3dSA":["pt","eta","phi","energy", 'showerlength', 'coreshowerlength', 'firstlayer','meanZQuotient', 'meanZFraction','meanZFraction', 'meanPhiQuotient', 'meanPhiFraction', 'meanEtaQuotient', 'meanEtaFraction', 'meanRoZQuotient', 'meanRoZFraction', 'sigmaEQuotient', 'sigmaEFraction', 'sigmaZQuotient', 'sigmaZFraction', 'sigmaPhiQuotient', 'sigmaPhiFraction', 'sigmaEtaQuotient', 'sigmaEtaFraction', 'sigmaRoZQuotient', 'sigmaRoZFraction', 'energyEMOverEnergyQuotient', 'energyEMOverEnergyFraction', 'energyEMCoreOverEnergyEMQuotient', 'energyEMCoreOverEnergyEMFraction', 'energyHEarlyOverEnergyQuotient', 'energyHEarlyOverEnergyFraction']
    }
}

# List of emulator variables to process
emulator_variables_to_process = ['meanZ', 'meanPhi', 'meanEta', 'meanRoZ', 'sigmaE', 'sigmaZ', 'sigmaPhi', 'sigmaEta', 'sigmaRoZ', 'energyEMOverEnergy', 'energyEMCoreOverEnergyEM', 'energyHEarlyOverEnergy']

# List of cluster variables to plot
variables_to_plot = {
    "simulation":[
        'pt', 'energy', 'eta', 'phi', 'showerlength', 'coreshowerlength', 'firstlayer','maxlayer', 'seetot', 'seemax', 'spptot', 'sppmax', 'szz', 'srrtot', 'srrmax', 'srrmean', 'emaxe', 'hoe', 'meanz', 'layer10', 'layer50', 'layer90', 'ntc67', 'ntc90', 'bdteg', 'quality'
    ],
    "emulator":[
        'pt', 'energy', 'eta', 'phi', 'showerlength', 'coreshowerlength', 'firstlayer', 'meanZ', 'meanPhi', 'meanEta', 'meanRoZ', 'sigmaE', 'sigmaZ', 'sigmaPhi', 'sigmaEta', 'sigmaRoZ', 'energyEMOverEnergy', 'energyEMCoreOverEnergyEM', 'energyHEarlyOverEnergy'
    ]
}
# Store all vars to plot in list
all_variables_to_plot = []
for k in variables_to_plot:
    for var in variables_to_plot[k]: 
      if var not in all_variables_to_plot: all_variables_to_plot.append(var)

cluster_key = {
    "simulation":"cl3d",
    "emulator":"cl3dSA"
}



# Containers to store event information
events = {}
cl3d_selected = {}
gen_selected = {}
cl3d_genmatched = {}
cl3d_nongenmatched = {}

from timeit import default_timer as timer
start_total = timer()

if load_collections:
    print(" --> Loading collections from analysis.pkl")
    with open("analysis.pkl", "rb") as fpkl: analysis = pickle.load(fpkl)
    events = analysis['events']
    gen_selected = analysis['gen_selected']
    cl3d_selected = analysis['cl3d_selected']
    cl3d_genmatched = analysis['cl3d_genmatched']
    cl3d_nongenmatched = analysis['cl3d_nongenmatched']


else:

    start = timer()

    for k,v in files.items():
        print(" --> Processing:", k)

        f = uproot.open(v)
        t = f["hgcalTriggerNtuplizer/HGCalTriggerNtuple"]

        # Print full set of tree branches
        print(" --> Input tree (%s) has variables:"%k, t.keys())

        # Extract all (required) event information into awkward array
        vars_to_store = []
        for var_type in variables[k.split("_")[0]]:
          for var in variables[k.split("_")[0]][var_type]:
             vars_to_store.append("%s_%s"%(var_type,var))
        vars_to_store.extend(['run','event','lumi'])
        events[k] = t.arrays(vars_to_store, library='ak', how='zip')

        # Extract cluster and gen particles in event as four vectors
        cl3d = ak.Array( events[k][cluster_key[k.split("_")[0]]], with_name="Momentum4D")
        gen = ak.Array( events[k]['gen'], with_name="Momentum4D")

        # Gen selection: pT > 2 GeV, 1.5 < |eta| < 3.0
        gen = gen[gen.pt>2]
        #gen = gen[gen.pt>2]
        gen = gen[(abs(gen.eta)>1.5)&(abs(gen.eta)<3.0)]
        # Save selected gen particles
        gen_selected[k] = gen

        # Cluster selection: pT > 2 GeV
        cl3d = cl3d[cl3d.pt>2]
        #cl3d = cl3d[cl3d.pt>2]

        # Give clusters unique cluster id
        cl3d_unique_id = []
        previous_id = 0
        for i in ak.to_list(ak.num(cl3d)):
            cl3d_unique_id.append( np.linspace(previous_id,previous_id+i-1,i, dtype='int') )
            previous_id += i
        cl3d['unique_id'] = ak.Array(cl3d_unique_id)

        # Save selected clusters
        cl3d_selected[k] = cl3d 

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Gen matching:
        # Make all combinations of clusters and gen particles
        gen_cl3d_pairs = ak.cartesian( {"gen":gen, "cl3d":cl3d} )
        # Extract args of gen and cl3d in pairs
        gen_cl3d_pairs_arg = ak.argcartesian( {"gen":gen, "cl3d":cl3d} )

        # Consider only clusters which have dR < 0.2 with gen particle
        dR_mask = gen_cl3d_pairs.gen.deltaR( gen_cl3d_pairs.cl3d ) < 0.2
        gen_cl3d_pairs_genmatched = gen_cl3d_pairs[dR_mask]
        gen_cl3d_pairs_arg_genmatched = gen_cl3d_pairs_arg[dR_mask]

        # If multiple clusters for gen particle, choose cl3d with highest pt
        # First order clusters by pt
        order_cl3d = ak.argsort(gen_cl3d_pairs_genmatched.cl3d.pt, ascending=False, axis=1)
        gen_cl3d_pairs_genmatched = gen_cl3d_pairs_genmatched[order_cl3d]
        gen_cl3d_pairs_arg_genmatched = gen_cl3d_pairs_arg_genmatched[order_cl3d]

        # Add event idx identifier to gen_cl3d pairs
        gen_cl3d_pairs_genmatched['ev_idx'] = np.linspace(0,len(gen_cl3d_pairs_genmatched)-1,len(gen_cl3d_pairs_genmatched), dtype='int')

        # Skim up to max number of gen particles
        n_gen_max = ak.max( ak.num(gen) )
        arr = []
        for i_gen in range( n_gen_max ):
          # Select clusters with same gen particle id
          mask = (gen_cl3d_pairs_arg_genmatched.gen == i_gen)
          # Find first cluster (highest pt) and append array to arr
          firsts = ak.firsts( gen_cl3d_pairs_genmatched[mask], axis=1 )
          # Remove nones
          firsts = firsts[~ak.is_none(firsts)]
          arr.append( firsts )

        # Concatenate array of gen matched clusters, order by event id and save
        cl3d_genmatched[k] = ak.concatenate(arr)
        cl3d_genmatched[k] = cl3d_genmatched[k][ ak.argsort( cl3d_genmatched[k].ev_idx, ascending=True ) ]
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
    end = timer()
    print(" --> Time elapsed processing events: %.3f s"%(end-start))


# Process emulator variables
# TODO: can play around with number of bits in frac here
for k in files:
    if 'emulator' in k:
        for var in emulator_variables_to_process:
            cl3d_selected[k][var] = cl3d_selected[k]['%sQuotient'%var]+cl3d_selected[k]['%sFraction'%var]/pow(2,8)


# Extract non-genmatched clusters
cl3d_genmatched_flat = {}
for k in files:
    cl3d_i_selected = np.array( ak.flatten( cl3d_selected[k].unique_id ) )
    cl3d_i_genmatched = np.array( cl3d_genmatched[k].cl3d.unique_id )
    mask_inverted = np.isin( cl3d_i_selected, cl3d_i_genmatched )
    cl3d_nongenmatched[k] = ak.flatten( cl3d_selected[k] )[~mask_inverted] 
    cl3d_genmatched_flat[k] = ak.flatten( cl3d_selected[k] )[mask_inverted]


# Save collections
if save_collections:
    print(" --> Saving collections to analysis.pkl")
    analysis = {
        "events" : events,
        "gen_selected" : gen_selected,
        "cl3d_selected" : cl3d_selected,
        "cl3d_genmatched": cl3d_genmatched,
        "cl3d_nongenmatched": cl3d_nongenmatched
    }
    with open("analysis.pkl", "wb") as fpkl: pickle.dump(analysis, fpkl)


# Plot cl3d_distributions
if do_cl3d_plots: 

    start = timer()

    for var in all_variables_to_plot:
        print(" --> Plotting: %s"%var)

        for k in files:

            if var not in variables_to_plot[k]: continue

            x = np.array( ak.flatten( cl3d_selected[k][var] ) )
            if norm_by_area:
                ext_str = "_normByArea"
                y_axis_title = "Fraction of clusters"
                w = np.ones_like(x)/len(x)
            elif norm_by_events:
                ext_str = "_normByEvents"
                y_axis_title = "# of clusters per event"
                w = np.ones_like(x)/len(events[k])
            else:
                ext_str = ""
                y_axis_title = "# of clusters"
                w = np.ones_like(x)

            if var in plotting_details:
                x_range = plotting_details[var]["xrange"] if "xrange" in plotting_details[var] else None
                nbins = plotting_details[var]["nbins"] if "nbins" in plotting_details[var] else 50
            else:
                x_range = None
                nbins = 50

            if x_range is not None:
                n = plt.hist(x, nbins, range=(x_range[0],x_range[1]), facecolor=color_map[k], alpha=0.5, label=k, weights=w)
            else:
                n = plt.hist(x, nbins, facecolor=color_map[k], alpha=0.5, label=k, weights=w)

            # Save range of histogram so subsequent files have same binning
            if var not in plotting_details:
                plotting_details[var] = {}
                plotting_details[var]["xrange"] = [ n[1].min(), n[1].max() ] 
                plotting_details[var]["nbins"] = len(n[1])-1

        # Set plotting options
        if var in plotting_details:
            if "logy" in plotting_details[var]: plt.yscale("log")
            
        plt.xlabel("Cluster %s"%var)
        plt.ylabel(y_axis_title)
        plt.grid(True)
        plt.legend()

        plt.savefig("%s/cl3d_%s%s.png"%(output_dir,var,ext_str))
        plt.savefig("%s/cl3d_%s%s.pdf"%(output_dir,var,ext_str))

        plt.clf()

    end = timer()
    print(" --> Time elapsed plotting cluster properties: %.3f s"%(end-start))


if do_sb_cl3d_plots: 

    start = timer()

    for var in all_variables_to_plot:

        for k in files:

            if var not in variables_to_plot[k]: continue

            print(" --> Plotting sig-vs-bkg: (%s, %s)"%(k,var))

            x_gm = np.array( cl3d_genmatched_flat[k][var] )
            x_ngm = np.array( cl3d_nongenmatched[k][var] ) 

            y_axis_title = "Fraction of clusters"
            w_gm = np.ones_like(x_gm)/len(x_gm)
            w_ngm = np.ones_like(x_ngm)/len(x_ngm)

            if var in plotting_details:
                x_range = plotting_details[var]["xrange"] if "xrange" in plotting_details[var] else None
                nbins = plotting_details[var]["nbins"] if "nbins" in plotting_details[var] else 50
            else:
                x_range = None
                nbins = 50

            if x_range is not None:
                n_gm =  plt.hist(x_gm, nbins, range=(x_range[0],x_range[1]), facecolor=color_map[k], alpha=0.5, label="%s (gen-matched)"%k, weights=w_gm)
                n_ngm = plt.hist(x_ngm, nbins, range=(n_gm[1].min(),n_gm[1].max()), facecolor=color_map["%s_ngm"%k], alpha=0.5, label="%s (PU)"%k, weights=w_ngm)
            else:
                n_gm = plt.hist(x_gm, nbins, facecolor=color_map[k], alpha=0.5, label="%s (gen-matched)"%k, weights=w_gm)
                n_ngm = plt.hist(x_ngm, nbins, range=(n_gm[1].min(),n_gm[1].max()), facecolor=color_map["%s_ngm"%k], alpha=0.5, label="%s (PU)"%k, weights=w_ngm)

            # Save range of histogram so subsequent files have same binning
            if var not in plotting_details:
                plotting_details[var] = {}
                plotting_details[var]["xrange"] = [ n_gm[1].min(), n_gm[1].max() ] 
                plotting_details[var]["nbins"] = len(n_gm[1])-1

            # Set plotting options
            if var in plotting_details:
                if "logy" in plotting_details[var]: plt.yscale("log")
            
            plt.xlabel("Cluster %s"%var)
            plt.ylabel(y_axis_title)
            plt.grid(True)
            plt.legend()

            plt.savefig("%s/sig_vs_bkg_%s_%s.png"%(output_dir,k,var))
            plt.savefig("%s/sig_vs_bkg_%s_%s.pdf"%(output_dir,k,var))

            plt.clf()

    end = timer()
    print(" --> Time elapsed plotting sig-vs-bkg cluster properties: %.3f s"%(end-start))



# Plot efficiency curve in bins of gen pt, gen eta
pt_cut = -1
pt_cut_ext = "" if pt_cut == -1 else ", gen $p_T$<%s GeV"%pt_cut
#label_map = {
#    "simulation":"simulation (0 PU)%s"%pt_cut_ext,
#    "emulator":"emulator (0 PU)%s"%pt_cut_ext,
#    "emulator_flat":"emulator, flat 0.5 GeV thr. (0 PU)"
#}

label_map = {
    "simulation":"simulation (200 PU)%s"%pt_cut_ext,
    "emulator":"emulator (200 PU)%s"%pt_cut_ext,
    "emulator_flat":"emulator, flat 0.5 GeV thr. (200 PU)%s"%pt_cut_ext
}


if do_efficiency_plot:

    start = timer()

    for var in ['pt','eta','phi']:
        print(" --> Plotting efficiency curve vs %s"%var)

        eff_genmatched, eff_genmatched_pass_egid = {}, {}

        for k in files:
            # Make three histograms: all gen, gen with matching cluster, gen with matching cluster passing egamma ID (quality==1)
            if pt_cut == -1:
                x = np.array( ak.flatten( gen_selected[k][var] ) )
            else:
                mask = gen_selected[k].pt < pt_cut
                x = np.array( ak.flatten( gen_selected[k][mask][var] ) )
            if var == "eta": x = abs(x)
            n_gen, bins = np.histogram(x, bins=plotting_details['efficiency_%s'%var]['bins'])

            if pt_cut == -1:
                x = np.array( cl3d_genmatched[k].gen[var] )
            else:
                mask = cl3d_genmatched[k].gen.pt < pt_cut
                x = np.array( cl3d_genmatched[k].gen[mask][var] )
            if var == "eta": x = abs(x)
            n_genmatched, bins = np.histogram( x, bins=plotting_details['efficiency_%s'%var]['bins'])
            # Divide histogram to get efficiency
            eff_genmatched[k] = n_genmatched/n_gen

            # If cluster does not possess quality (flag for cluster passing egid) then set to zero
            if "quality" not in ak.fields(cl3d_genmatched[k].cl3d):
                eff_genmatched_pass_egid[k] = np.zeros_like(eff_genmatched[k])
            else:
                cl3d_passing_egid = cl3d_genmatched[k][ cl3d_genmatched[k].cl3d.quality == 1 ]
                if pt_cut == -1:
                    x = np.array( cl3d_passing_egid.gen[var] )
                else:
                    mask = cl3d_passing_egid.gen.pt < pt_cut
                    x =  np.array( cl3d_passing_egid.gen[mask][var] )
                if var == "eta": x = abs(x)
                n_genmatched_pass_egid, bins = np.histogram( x, bins=plotting_details['efficiency_%s'%var]['bins'])

                # Divide histograms to get efficiencies
                eff_genmatched_pass_egid[k] = n_genmatched_pass_egid/n_gen

            # Add repeat value to end of array (step plotting in matplotlib)
            eff_genmatched[k] = np.append(eff_genmatched[k],eff_genmatched[k][-1])
            eff_genmatched_pass_egid[k] = np.append(eff_genmatched_pass_egid[k],eff_genmatched_pass_egid[k][-1])

            plt.step( plotting_details['efficiency_%s'%var]['bins'], eff_genmatched[k], color=color_map[k], label=label_map[k], where='post')
            #plt.step( plotting_details['efficiency_%s'%var]['bins'], eff_genmatched_pass_egid[k], color=color_map[k], linestyle='dashed', label="%s (passing ID)"%k, where='post')

        plt.xlabel("Gen %s"%var)
        plt.ylabel("Efficiency")
        plt.ylim(0,1.1)
        plt.grid()
        plt.legend()

        if pt_cut == -1: ext_str = ""
        else: ext_str = "_ptcut%s"%pt_cut

        plt.savefig("%s/efficiency_vs_gen_%s%s.png"%(output_dir,var,ext_str))
        plt.savefig("%s/efficiency_vs_gen_%s%s.pdf"%(output_dir,var,ext_str))

        plt.clf()

    end = timer()
    print(" --> Time elapsed plotting efficiencies: %.3f s"%(end-start))
        

# Plot resolution distributions
if do_resolution_plot:

    start = timer()

    print(" --> Plotting cluster resolution")

    # Containers to store mean and std of resolution
    mean, std = {}, {}

    # Loop over pT ranges
    n_pt_bins = len(plotting_details['resolution']['pt_bins'])-1

    for i_pt_bin in range(n_pt_bins):

        pt_min = plotting_details['resolution']['pt_bins'][i_pt_bin]
        pt_max = plotting_details['resolution']['pt_bins'][i_pt_bin+1]
        pt_bin_str = "pt_%g_to_%g"%(pt_min,pt_max)
        mean[pt_bin_str] = {}
        std[pt_bin_str] = {}

        for k in files:

            # Extract gen matched clusters pt in pt range
            mask = ( cl3d_genmatched[k].gen.pt >= pt_min )&( cl3d_genmatched[k].gen.pt < pt_max )
            gen_pt = cl3d_genmatched[k][mask].gen.pt
            cl3d_pt = cl3d_genmatched[k][mask].cl3d.pt
            res = np.array( (cl3d_pt-gen_pt)/gen_pt )

            # Weight histogram: area = 1
            w = np.ones_like(res)/len(res)

            plt.hist( res, bins=plotting_details['resolution']['nbins'], range=(-0.5,0.5), facecolor=color_map[k], alpha=0.5, label=k, weights=w )

            # TODO: add gaussian fit to extract mean and std when enough events
            # Also add error on the values: plot with fill_between
            mean[pt_bin_str][k] = ak.mean(res)
            std[pt_bin_str][k] = ak.std(res)
       

        plt.xlabel("( pT(cl3d) - pT(gen) ) / pT(gen)")
        plt.ylabel("Fraction of clusters")
        plt.grid()
        plt.legend()

        plt.savefig("%s/resolution_%s.png"%(output_dir,pt_bin_str))
        plt.savefig("%s/resolution_%s.pdf"%(output_dir,pt_bin_str))

        plt.clf()

    # Do summary plots: mean and std
    mean_steps = {}
    std_steps = {}
    for k in files:

        mean_steps[k] = []
        std_steps[k] = []

        for i_pt_bin in range(n_pt_bins):

            pt_min = plotting_details['resolution']['pt_bins'][i_pt_bin]
            pt_max = plotting_details['resolution']['pt_bins'][i_pt_bin+1]
            pt_bin_str = "pt_%g_to_%g"%(pt_min,pt_max)

            mean_steps[k].append( mean[pt_bin_str][k] )
            std_steps[k].append( std[pt_bin_str][k] )

        # Repeat final element, for matplotlib steps and convert to numpy array
        mean_steps[k].append( mean_steps[k][-1] )
        std_steps[k].append( std_steps[k][-1] )
        mean_steps[k] = np.array( mean_steps[k] )
        std_steps[k] = np.array( std_steps[k] )

    # Mean
    for k in files: plt.step( plotting_details['resolution']['pt_bins'], mean_steps[k], color=color_map[k], label=k, where='post')
    plt.xlabel("pT(gen)")
    plt.ylabel("Mean pT response")
    plt.ylim(plotting_details['resolution']['mean_yrange'])
    plt.grid()
    plt.legend()

    plt.savefig("%s/resolution_response_mean_pt.png"%(output_dir))
    plt.savefig("%s/resolution_response_mean_pt.pdf"%(output_dir))

    plt.clf()

    # Std
    for k in files: plt.step( plotting_details['resolution']['pt_bins'], std_steps[k], color=color_map[k], label=k, where='post')
    plt.xlabel("pT(gen)")
    plt.ylabel("STD pT response")
    plt.ylim(plotting_details['resolution']['std_yrange'])
    plt.grid()
    plt.legend()

    plt.savefig("%s/resolution_response_std_pt.png"%(output_dir))
    plt.savefig("%s/resolution_response_std_pt.pdf"%(output_dir))

    plt.clf()

    end = timer()
    print(" --> Time elapsed plotting resolution: %.3f s"%(end-start))


# Plot pt-response distributions
if do_ptresponse_plot:

    start = timer()

    print(" --> Plotting pt-response distribution")

    for k in files:

        # Extract gen matched clusters pt in pt range
        gen_pt = cl3d_genmatched[k].gen.pt
        cl3d_pt = cl3d_genmatched[k].cl3d.pt
        res = np.array( cl3d_pt/gen_pt )

        # Weight histogram: area = 1
        w = np.ones_like(res)/len(res)

        plt.hist( res, bins=50, range=(0.5,1.5), facecolor=color_map[k], alpha=0.5, label=label_map[k], weights=w )

       

    plt.xlabel("pT(cl3d) / pT(gen)")
    plt.ylabel("Fraction of clusters")
    plt.grid()
    plt.legend()

    plt.savefig("%s/pt_response.png"%output_dir)
    plt.savefig("%s/pt_response.pdf"%output_dir)

    plt.clf()

    end = timer()
    print(" --> Time elapsed plotting resolution: %.3f s"%(end-start))


# Plot eta-phi distributions
if do_sandbox_plot:

    start = timer()

    print(" --> Plotting sandbox")

    # Extract selected gen particle eta/phi for pt < pt_cut GeV
    gps = ak.flatten(gen_selected['simulation'][gen_selected['simulation'].pt<pt_cut])

    gps_matched_simulation = cl3d_genmatched['simulation'][cl3d_genmatched['simulation'].gen.pt<pt_cut].gen
    gps_matched_emulator = cl3d_genmatched['emulator'][cl3d_genmatched['emulator'].gen.pt<pt_cut].gen

    plt.scatter(abs(np.array(gps.eta)), np.array(gps.phi), s=20, c='grey', label='All gen particles ($p_{T}$<%s GeV)'%pt_cut)
    plt.scatter(abs(np.array(gps_matched_simulation.eta)), np.array(gps_matched_simulation.phi), s=10, c=color_map['simulation'], label='Simulation matched')
    plt.scatter(abs(np.array(gps_matched_emulator.eta)), np.array(gps_matched_emulator.phi), s=5, c=color_map['emulator'], label='Emulator matched')

    plt.xlabel("|gen eta|")
    plt.ylabel("gen phi")
    plt.grid()
    plt.legend()

    plt.xlim(1.4,3.1)
    plt.ylim(-3.2,3.2)

    plt.savefig("%s/gen_eta_vs_phi_ptcut%s.pdf"%(output_dir,pt_cut))
    plt.savefig("%s/gen_eta_vs_phi_ptcut%s.png"%(output_dir,pt_cut))

    plt.clf()

    end = timer()
    print(" --> Time elapsed plotting sandbox: %.3f s"%(end-start))

    
    

print("*******************************************************")
end_total = timer()
print(" --> Total time elapsed: %.3f s"%(end_total-start_total))
