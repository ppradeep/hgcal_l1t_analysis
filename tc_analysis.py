import os
import uproot
import awkward as ak
import pickle
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import vector
vector.register_awkward()

do_cl3d_plots = True

if do_cl3d_plots:
    output_dir = "./plots_ptcut2_rho_studies"
    if not os.path.isdir(output_dir):
        os.system("mkdir -p %s"%output_dir)

variables = {
    "simulation":{
        "tc":["id","pt","x","y","z","phi","multicluster_id"],
        "cl3d":["n","id","pt","rhoROverZvsZ","rhoPhivsZ"]
    }
}

cluster_key = {
    "simulation":"cl3d",
    "emulator":"cl3dSA"
}


# Uproot ntuple produced from HGCAL L1T TPG software
files = {}
files['simulation'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/ntuple_200PU_test.root"

# Containers to store event information
events = {}
cl3d = {}

# Function to calculate corr coefficient
def correlation(n,x,y):
    N = n*(ak.sum(x*y))-ak.sum(x)*ak.sum(y) 
    if( (n*ak.sum(x*x)-ak.sum(x)*ak.sum(x)) < 0 )|( (n*ak.sum(x*x)-ak.sum(x)*ak.sum(x)) < 0 ): return -999
    D = np.sqrt(n*ak.sum(x*x)-ak.sum(x)*ak.sum(x))*np.sqrt(n*ak.sum(y*y)-ak.sum(y)*ak.sum(y))
    if D == 0: return -999
    return N/D

from timeit import default_timer as timer
start_total = timer()

start = timer()

for k,v in files.items():
    print(" --> Processing:", k)

    f = uproot.open(v)
    t = f["hgcalTriggerNtuplizer/HGCalTriggerNtuple"]

    # Print full set of tree branches
    print(" --> Input tree (%s) has variables:"%k, t.keys())

    # Extract all (required) event information into awkward array
    vars_to_store = []
    for var_type in variables[k]:
        for var in variables[k][var_type]:
            vars_to_store.append("%s_%s"%(var_type,var))
    events[k] = t.arrays(vars_to_store, library='ak', how='zip')

    cl3d[k] = events[k]['cl3d'][events[k]['cl3d'].pt>2]

    # Loop over events
    N_ev = len(events[k])
    
    RHO_R_OVER_Z_VS_Z = []
    RHO_PHI_VS_Z = []

    for i_ev in range(N_ev):
        event = events[k][i_ev] 

        rho_r_over_z_vs_z = []
        rho_phi_vs_z = []

        N_cl3d = len(cl3d[k][i_ev])

        for i_cl3d in range(N_cl3d):
            # Mask for trigger cells in cluster
            cl3d_id = cl3d[k][i_ev][i_cl3d]['id']

            # Find trigger cells in cluster
            mask = event['tc']['multicluster_id']==cl3d_id
            tcs = event['tc'][mask]

            N = len(tcs)
            x, y, z = tcs.x, tcs.y, tcs.z
            phi = tcs.phi
            r = np.sqrt(x**2+y**2)
            r_over_z = r/abs(z)

            # TODO: Add scatter plot for few example events
            rho_r_over_z_vs_z.append(correlation(N,r_over_z,abs(z)))
            rho_phi_vs_z.append(correlation(N,phi,abs(z)))

        RHO_R_OVER_Z_VS_Z.append(ak.Array(rho_r_over_z_vs_z))
        RHO_PHI_VS_Z.append(ak.Array(rho_phi_vs_z))

    cl3d[k]['rho_RoverZ_vs_Z'] = ak.Array( RHO_R_OVER_Z_VS_Z )
    cl3d[k]['rho_phi_vs_Z'] = ak.Array( RHO_PHI_VS_Z )

end = timer()
print(" --> Time elapsed processing events: %.3f s"%(end-start))

# Extract tcs from cluster: 3127792213
mask = events['simulation']['tc']['multicluster_id']==3127792213
tcs = events['simulation']['tc'][mask][0]
print (" --> [DEBUG] Found cluster (3127792213). Printing trigger cell info")
# Sort tcs by id
tcs_id_sort = np.argsort(tcs.id)
tcs_roverz = (np.sqrt(tcs.x**2+tcs.y**2)/abs(tcs.z))[tcs_id_sort]
tcs_z = tcs.z[tcs_id_sort]
tcs_phi = tcs.phi[tcs_id_sort]
tcs_id = tcs.id[tcs_id_sort]
n_tcs = len(tcs_id)
for i_tc in range(n_tcs):
    print("  * tc:%s --> r/z = %.5f, z = %.5f, phi = %.5f"%(tcs_id[i_tc],tcs_roverz[i_tc],abs(tcs_z[i_tc]),tcs_phi[i_tc]))



# Plot cl3d_distributions
all_variables_to_plot = ['rho_RoverZ_vs_Z','rho_phi_vs_Z']

variable_to_variable = {
    "rho_RoverZ_vs_Z":"rhoROverZvsZ",
    "rho_phi_vs_Z":"rhoPhivsZ"
}

if do_cl3d_plots:

    for var in all_variables_to_plot:
        print(" --> Plotting: %s"%var)

        for k in files:

            x = np.array( ak.flatten( cl3d[k][var] ) )
            x_comp = np.array( ak.flatten( cl3d[k][variable_to_variable[var]] ) )
            w = np.ones_like(x)/len(x)

            nbins = 50
            n = plt.hist(x, nbins, range=(-1,1), facecolor='deepskyblue', alpha=0.5, label="Calculated", weights=w)
            n_comp = plt.hist(x_comp, nbins, range=(-1,1), facecolor='lightcoral', alpha=0.5, label="CMSSW variable", weights=w)

        plt.xlabel("Cluster %s"%var)
        plt.ylabel("Fraction of clusters")
        plt.grid(True)
        plt.legend()

        plt.savefig("%s/cl3d_%s.png"%(output_dir,var))
        plt.savefig("%s/cl3d_%s.pdf"%(output_dir,var))

        plt.clf()
    

