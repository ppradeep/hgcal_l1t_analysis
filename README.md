# HGCAL L1 Trigger Analysis Code

A collection of scripts to analyse output of the HGCAL L1T TPG (trigger primitive generator) software. First step is to clone this repository:

```
git clone https://gitlab.cern.ch/jlangfor/hgcal_l1t_analysis.git
cd hgcal_l1t_analysis
```

## Installation

Analysis scripts depend on a number of python packages. Suggested way is to create a conda environment. If you need to install the miniconda (version 3) package then run:
```
source install_conda.sh
```
To automatically initiate conda upon loading the terminal, please add the first line into your `~/.bashrc` file (with the correct path if this was changed when installing the conda package). For users on the Imperial clusters, to avoid issues with python versions you may also need to add the second line into your `~/.bashrc`:
```
source /vols/cms/${USER}/miniconda3/etc/profile.d/conda.sh
unset PYTHONPATH
```

When installed you need to build the environment with all the dependencies with the following command:
```
conda env create -f environment.yml
```
Then activate the environment with
```
conda activate hgcal-l1t-analysis
```

## Running the code

To run a simple analysis:
```
python test.py
```
This script will perform a basic selection on the clusters generated by the [TPG software](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HGCALTriggerPrimitivesSimulation). In addition, the script performs a gen particle matching to evaluate the efficiency of reconstructing a particle in the HGCAL trigger. The `test.py` script adopts a columnar analysis technique, which is more efficient than the equivalent "event-loop" based analysis (`test_loop.py`, script is now not up to-date with test.py but gives an example of how you can write a loop-based analysis). The options for the script include:
  *  `save_collections`: saves event information, selected gen particles, selected clusters and gen-matched clusters in a pickle file (`analysis.pkl`).
  *  `load_collections`: skip event processing step, and load information directly from `analysis.pkl`.
  *  `do_cl3d_plots`: plot reconstructed cluster properties.
  *  `do_sb_cl3d_plots`: plot cluster properties for gen-matched clusters vs PU (non-gen-matched)
  *  `do_efficiency_plot`: plot efficiency of reconstructing cluster as a function of the generator-level particle pt and eta. The efficiency is also plotted for clusters which pass the e/gamma identification BDT.
  *  `do_resolution_plot`: plot the resolution of the reconstructed clusters = (pt_reco - pt_true)/pt_true. This is plotted in bins of the generator level particle pt (pt_true). The mean and width of the corresponding distributions are also plotted, as a function of pt_true.

A number of the plotting options can be adapted at the top of the script.

The script can run over multiple ntuples generated by the TPG software. Currently, the script is configured to run over ntuples produced using both the standard simulation in CMSSW, and also with the firmware emulation. The ntuples contain >6000 events, and can be found here:
```
files['simulation'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_simulation/ntuple_simulation_skimmed.root"
files['emulator'] = "/vols/cms/jl2117/postdoc/upgrade/hgcal/emulator/Feb22/samples/DoublePhoton_FlatPt-1To100_PU200_newprocessors/ntuple_newprocessors_skimmed.root"
```
The outputs from the two ntuples will be overlaid with eachother in the output plots (where possible). Example plots can be found [here](https://jlangfor.web.cern.ch/jlangfor/CMS/postdoc/upgrade/hgcal/emulator/Mar22/ptcut2/).

